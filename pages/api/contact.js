import axios from "axios";

export default async function handler(req, res) {
  const { email, message } = req.body;

  try {
    const result = await axios.post(
      "https://api.brevo.com/v3/smtp/email",
      {
        sender: {
          name: "Crew Labs",
          email: "crew@crewlabs.io",
        },
        to: [
          {
            email: process.env.RECIPIENT_EMAIL_ADDRESS,
          },
        ],
        subject: "Message for crewlabs.io",
        htmlContent: `
      <html>
      <head>
      <style>
      .container {
        width: 100%;
        max-width: 600px;
        margin: 0 auto;
        padding: 0 20px;
      }
      .header {
        background-color: #f1f1f1;
        padding: 30px 10px;
      }
      .header h1 {
        text-align: center;
        font-size: 30px;
        color: #000;
        margin: 0;
      }
      .content {
        padding: 20px 10px;
        background-color: #fff;
      }
      .content p {
        font-size: 16px;
        color: #000;
        margin: 0;
      }
      .footer {
        background-color: #f1f1f1;
        padding: 30px 10px;
      }
      .footer p {
        font-size: 16px;
        color: #000;
        margin: 0;
      }
      </style>
      </head>
      <body>
      <div class="container">
        <div class="header">
          <h1>Message for crewlabs.io</h1>
        </div>
        <div class="content">
          <p>Message: ${message}</p>
          <p>From: ${email}</p>
        </div>
      </div>
      </body>
      </html>
      `,
      },
      {
        headers: {
          "Content-Type": "application/json",
          Accept: "application/json",
          "api-key":
            "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",
        },
      }
    );

    console.log(result.data);

    return res.status(200).json({
      success: true,
      message: "Message sent!",
    });
  } catch (error) {
    console.log(error);
    return res.status(500).json({
      success: false,
      message: "Something went wrong!",
    });
  }
}
