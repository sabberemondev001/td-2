import Contact from "@/components/Contact/Contact";
import Features from "@/components/Features/Features";
import Footer from "@/components/Footer/Footer";
import Header from "@/components/Header/Header";
import Services from "@/components/Services/Services";
import Sponsors from "@/components/Sponsors/Sponsors";
import Team from "@/components/Team/Team";
import "animate.css";
import Head from "next/head";
import styles from "../styles/Header.module.css";
import Partners from "@/components/Partners/Partners";
export default function Home() {
  return (
    <>
      <Head>
        <title>Crew Labs - The Future of Web</title>
        <meta name="description" content="Crew Labs - The Future of Web" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.heading}>
        <Header />
        <Partners />
        <Sponsors />
        <Features />
        <div className="team_services_section">
          <Services />
          <Team />
        </div>
        <Contact />
        <Footer />
      </main>
    </>
  );
}
