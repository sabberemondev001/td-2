import icon1 from "../images/sponsors/team/images10.svg";
import icon2 from "../images/sponsors/team/images11.svg";
import icon3 from "../images/sponsors/team/images12.svg";
import icon4 from "../images/sponsors/team/images13.svg";

const sponsers = [
  {
    id: 1,
    icon: icon1,
  },
  {
    id: 2,
    icon: icon2,
  },
  {
    id: 3,
    icon: icon3,
  },
  {
    id: 4,
    icon: icon4,
  },
];

export default sponsers;
