import data from "./data";

// import Image from "next/image";

const Sponsors = () => {
  return (
    <div
      className="px-6 md:px-24 pt-2 md:pt-0"
      data-aos="zoom-in"
      data-aos-duration="2000"
    >
      <p className="title">Partnerships</p>
      <div className="flex pb-4 my-5 items-center gap-7 flex-wrap w-full md:w-[65%] mx-auto">
        {data.map((img) => (
          <span className="mx-auto" key={img.id}>
            <img.icon style={{ width: 200, height: 35 }} />
            {/* <Image src={img.icon} alt="team logo" width={200} height={200} /> */}
          </span>
        ))}
      </div>
    </div>
  );
};

export default Sponsors;

// grid pb-4 my-5 mt-10 md:grid-cols-4 grid-cols-1 gap-7 w-full md:w-[60%] mx-auto
