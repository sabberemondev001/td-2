import { useState } from "react";
import toast from "react-hot-toast";
import axios from "axios";
import styles from "../../styles/Contact.module.css";
import Button from "../Button/Button";
import Input from "../Input/Input";

const Contact = () => {
  const [loading, setLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    // get data from form
    const formData = new FormData(e.target);

    const email = formData.get("email");
    const message = formData.get("message");

    if (!email || !message) return toast.error("Please fill all the fields!");

    // test if the email is valid
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!emailRegex.test(email)) {
      setLoading(false);
      return toast.error("Please enter a valid email address!");
    }

    const data = {
      email,
      message,
    };

    // promise based toast.promise
    const notify = toast.promise(axios.post(`/api/contact`, data), {
      loading: "Plese wait...",
      success: (res) => {
        setLoading(false);
        return res.data.message;
      },
      error: (err) => {
        setLoading(false);
        return err.response.data.message;
      },
    });

    await notify;

    // reset the form
    e.target[0].value = "";
    e.target[1].value = "";
  };

  return (
    <div
      id="contact-us"
      className="grid overflow-x-hidden grid-cols-1 py-8 container mx-auto lg:px-16  md:grid-cols-2 px-6 md:px-24"
    >
      <div
        className="my-10 md:my-0"
        data-aos="fade-right"
        data-aos-duration="1000"
      >
        <p className={`${styles.title}`}>Questions, bug reports, feedback.</p>
        <h2 className={`${styles.heading}`}>Contact us</h2>
      </div>
      <form
        className="mb-4"
        data-aos="fade-left"
        data-aos-duration="1000"
        onSubmit={handleSubmit}
      >
        <div className="">
          <label className={styles.title} htmlFor="Email">
            Your email
          </label>
          <Input />
        </div>
        <div>
          <label className={`${styles.title}`} htmlFor="message">
            Tell us what you need help with:
          </label>
          <textarea
            className={styles.message}
            id="message"
            name="message"
            cols="60"
            rows="6"
            placeholder="Enter your message"
          ></textarea>
        </div>
        <div className="mt-4">
          <Button
            name={loading ? "Sending..." : "Send now"}
            btnType="submit"
            disabled={loading}
          />
        </div>
      </form>
    </div>
  );
};

export default Contact;
