import icon1 from "../images/sponsors/team/images5.svg";
import icon2 from "../images/sponsors/team/images6.svg";
import icon3 from "../images/sponsors/team/images7.svg";
import icon4 from "../images/sponsors/team/images8.svg";
import icon5 from "../images/sponsors/team/images9.svg";

const partners = [
  {
    id: 1,
    icon: icon1,
    w: 272,
    h: 50,
  },
  {
    id: 2,
    icon: icon2,
    w: 257,
    h: 73,
  },
  {
    id: 3,
    icon: icon3,
    w: 260,
    h: 50,
  },
  {
    id: 4,
    icon: icon4,
    w: 270,
    h: 40,
  },
  {
    id: 5,
    icon: icon5,
    w: 350,
    h: 48,
  },
];

export default partners;
