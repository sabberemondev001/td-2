import data from "./data";

const Partners = () => {
  return (
    <div
      className="px-6 md:px-24 pt-2 md:pt-0"
      data-aos="fade-up"
      data-aos-duration="1500"
    >
      <p className="title">Project made by the Crew</p>
      <div className=" flex pb-4 my-5 items-center gap-0 md:gap-4 lg:gap-8 justify-center flex-col md:flex-row md:flex-wrap w-full md:w-[65%] mx-auto">
        {data.map((img) => (
          <img.icon
            key={img.id}
            className=""
            style={{ width: 280, height: 85 }}
          />
        ))}
      </div>
    </div>
  );
};

export default Partners;
