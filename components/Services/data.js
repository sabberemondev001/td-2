import image1 from "../images/services/icon1.svg";
import image2 from "../images/services/icon2.svg";
import image3 from "../images/services/icon3.svg";
import image4 from "../images/services/icon4.svg";
import image5 from "../images/services/icon5.svg";
import image6 from "../images/services/icon6.svg";
import image7 from "../images/services/icon7.svg";
import image8 from "../images/services/icon8.svg";

const servicesData = [
    {    id : 1,
        title : "Protect Your Data",
        descripetion : "Help safeguard your personal information, privacy, and security, minimizing the risk of identity theft, fraud, and unauthorized access.",
        image : image1
    },
    {    id : 2,
        title : `Super Smart Search`,
        descripetion : "Deliver highly accurate and relevant search results by understanding context, user intent, and providing intelligent suggestions.",
        image : image2
    },
    {    id : 3,
        title : "Control",
        descripetion : "Enables effective decision-making, ensures accountability, mitigates risks, and facilitates the achievement of organizational goals.",
        image : image3
    },
    {    id : 4,
        title : "Work better together",
        descripetion : "Collaboration fosters innovation, enhances problem-solving capabilities, increases productivity, and strengthens relationships, ultimately leading to collective success.",
        image : image4
    },
    {    id : 5,
        title : "Be Autonomous",
        descripetion : "Promote self-reliance, empowers individuals to make independent decisions, encourages creativity and personal growth, and allows for a sense of ownership and fulfillment in one's actions.",
        image : image5
    },
    {    id : 6,
        title : "Manage Projects",
        descripetion : "Ensure efficient resource allocation, timely delivery of results, effective communication and coordination, and overall project success while minimizing risks and maximizing productivity.",
        image : image6
    },
    {    id : 7,
        title : "Communicate",
        descripetion : "Build strong relationships, promotes teamwork, facilitates problem-solving, and ensures alignment of goals and expectations, leading to successful collaboration and overall organizational success.",
        image : image7
    },
    
    {    id : 8,
        title : "Decentralize",
        descripetion : "Reduce dependency on a single point of failure, ultimately enabling resilience, agility, and distributed ownership within an organization or system.",
        image : image8
    },
   
]
export default servicesData