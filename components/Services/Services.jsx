import styles from "../../styles/Services.module.css";
import data from "./data";
const Services = () => {
  return (
    <div
      id="about-us"
      className={`px-[2rem] lg:px-16 pt-10 md:pt-0 pb-24 md:px-[8rem] container mx-auto ${styles.bg}`}
    >
      <div className="px-4 md:px-20 mx-auto">
        <div
          className="heading pb-10"
          data-aos="fade-up"
          data-aos-duration="2000"
        >
          <p className="title">thanks to web 3.0</p>
          <h3 className="heading">We're able to</h3>
        </div>
      </div>
      <div className="grid grid-cols-1 gap-10 md:grid-cols-4">
        {data.map((item) => (
          <div key={item.id} data-aos="fade-down" data-aos-duration="2000">
            <item.image className="text-4xl my-1" />
            <p className={styles.title}>{item.title}</p>
            <div className={styles.line}></div>
            <small className={styles.description}>{item.descripetion}</small>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Services;
