import styles from "../../styles/Card.module.css";
import Button from "../Button/Button";

import Icon3 from "../images/Crew Labs SVG/Shine-3.svg";
import Card from "./Card";
// lg:ml-[4.75rem]
// md:px-24
const Features = () => {
  return (
    <div
      id="web3"
      className={`block md:flex items-center  container mx-auto lg:px-2 justify-evenly md:py-28 px-4 md:px-0`}
    >
      <div className={`basic-[47%] `}>
        <div className="block lg:flex items-center humsufer gap-5 m-[10px] md:ml-0 md:m-[45px]">
          <div
            className="grid grid-cols-1 gap-7"
            data-aos="fade-right"
            data-aos-easing="ease-in-sine"
            data-aos-duration="1000"
          >
            <Card />
          </div>
          <div
            className="pt-8 ml-0 lg:ml-[5px]"
            data-aos="fade-down"
            data-aos-easing="ease-in-sine"
            data-aos-duration="1000"
          >
            <div
              className={`${styles.CardStyle} w-[100%] py-12 md:w-[100%] text-center`}
            >
              <div className="w-full md:w-[70%] mx-auto">
                <div className={styles.cardIcon}>
                  <Icon3 className="text-[4rem] mx-auto" />
                </div>
                <p className="py-4 text-2xl md:text-xl">Decentralized</p>
                <span className="w-[80%] mx-auto md:w-full block md:text-sm  text-[#AEAFB4]">
                  Where data and services are distributed among the network
                  rather than controlled by a central authority.
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className="basis-[52%] px-3 mt-14"
        data-aos="fade-left"
        data-aos-easing="ease-in-sine"
        data-aos-duration="1000"
      >
        <h2 className="features_title mt-10 md:mt-0">
          Unleash the Power
          <br className="hidden md:block " /> of the new Digital
          <br className="hidden md:block " />
          Age with Web 3.0
        </h2>
        <p className="text-[#AEAFB4] text-sm my-6">
          Where control is distributed among its users rather than centralized
          in a few large companies or organizations. De-Fi will disrupt
          traditional financial systems by enabling new business models and
          opportunities for entrepreneurship.
        </p>
        {/* <Button name="Learn more" width="w-full md:w-[40%]" /> */}
      </div>
    </div>
  );
};

export default Features;
