import styles from "../../styles/Card.module.css";
import blockchainImg from "../images/Crew Labs SVG/Shine-1.svg";
import icon2 from "../images/Crew Labs SVG/Shine-2.svg";
const Card = () => {
  const data = [
    {
      id: 1,
      title: "Ahead of the Curve",
      subTitle:
        "Revolutionize business models by reducing intermediaries, costs, and increasing transparency.",
      icon: blockchainImg,
    },
    {
      id: 2,
      title: "Creative",
      subTitle:
        "A more open and accessible web, allowing for new forms of creativity, and enabling new opportunities.",
      icon: icon2,
    },
  ];

  return (
    <>
      {data.map((data) => (
        <div
          key={data.id}
          className={`${styles.CardStyle} w-full py-12 md:w-[100%] text-center`}
        >
          <div className="w-full md:w-[70%] mx-auto ">
            <div className={styles.cardIcon}>
              {<data.icon className="text-[4rem] mx-auto" />}
            </div>
            <div className="overflow-x-hidden">
              <p className="py-4 text-2xl md:text-xl">{data.title}</p>
              <span className="w-[80%] mx-auto md:w-full block md:text-sm text-[#AEAFB4]">
                {data.subTitle}
              </span>
            </div>
          </div>
        </div>
      ))}
    </>
  );
};

export default Card;
