import styles from "../../styles/Input.module.css";
const Input = ({ btnName, placeholder, height, ...rest }) => {
  return (
    <div className={`  ${styles.inputGroup} ${height} justify-between`}>
      <input
        className="bg-transparent border-0 outline-0 w-full"
        type="email"
        name="email"
        placeholder="Enter you email address"
        {...rest}
      />
    </div>
  );
};

export default Input;
