import Image from "next/image";
import Link from "next/link";
import styles from "../../styles/Team.module.css";
// import Anne from "../images/team/Anne.png";
// import Cassey from "../images/team/Cassey.png";
// import Christian from "../images/team/Christian.png";
// import Dan from "../images/team/Dan.png";
import Jack from "../images/team/Jack.png";
// import Linkedin from "../images/team/linkedien.svg";
import Steve from "../images/team/Steve.png";
// import Twitter from "../images/team/twitter.svg";
import Veronica from "../images/team/Veronica.png";
import Wanda from "../images/team/Wanda.png";

const Team = () => {
  const teamMembers = [
    {
      id: 1,
      name: "Ryan Robertson",
      role: "Software Developer",
      img: Wanda,
    },
    {
      id: 2,
      name: "Alex Hamilton",
      role: "UI/UX Specialist & Creative Director",
      img: Steve,
    },
    {
      id: 3,
      name: "Thomas Gaffney",
      role: "Business Strategy & Development, Attorney",
      img: Jack,
    },
    {
      id: 4,
      name: "Soumik Dey",
      role: "Project and Resource Manager",
      img: Veronica,
    },
  ];
  return (
    <section id="team">
      <div className="relative pt-2  container mx-auto">
        <div className={` px-6 md:px-20 mx-auto`}>
          <div className="heading" data-aos="fade-up" data-aos-duration="3000">
            <p className="title">Who we are?</p>
            <h2 className="heading">Meet our Founders</h2>
          </div>
          <div
            className="grid pt-8 gap-5 md:my-8 grid-cols-2 items-center md:grid-cols-4"
            data-aos="zoom-in"
            data-aos-duration="3000"
          >
            {teamMembers.map((item) => (
              <div key={item.id} className={`text-center py-4 md:py-10`}>
                <Image
                  className="rounded-full w-[106px]  md:w-[130px] h-[106px] md:h-[130px] block mx-auto"
                  alt={item.name}
                  src={item.img}
                />
                <div className="mt-8">
                  <h2 className={`${styles.name}`}>{item.name}</h2>
                  <p className={`${styles.role}`}>{item.role}</p>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Team;
