import Link from "next/link";
import styles from "../../../styles/Hero.module.css";
const Hero = () => {
  return (
    <div className="pt-0 pb-[3rem] md:pt-10">
      <div
        className="px-6 md:px-24 grid grid-cols-1"
        data-aos="zoom-out"
        data-aos-duration="1000"
      >
        <div className="pt-[4rem] md:pt-5 text-left md:text-center  w-full lg:w-[84%] mx-auto">
          <h2 className={`${styles.heading}`}>
            <span className={`${styles.theText}`}>The</span>{" "}
            <span className={`${styles.defiText}`}>DeFi</span>{" "}
            <br className="block md:hidden" />
            {/* <span className="text-[#004BDC] md:text-[36A6F6]">Crew</span> */}
            <span className={`${styles.crewText}`}>Crew</span>
          </h2>
          <p
            className={`${styles.subHeading} text-base py-4 w-full lg:w-[90%] mx-auto`}
          >
            Re-DeFining the future of finance with Crew Lab’s business
            solutions. We decentralize your business by unleashing the power of
            Web 3.0 and maximizing your performance.
          </p>
          <div className="flex w-full m-0 md:mx-auto md:w-[40%] items-center justify-center text-base py-14 gap-[28px]">
            <div>
              <Link className={`${styles.btn}`} href="#contact-us">
                Get Started Now
              </Link>
            </div>
            <p> </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Hero;
